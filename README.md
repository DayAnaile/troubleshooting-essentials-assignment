# Troubleshooting Essentials Assignment

A small application to collect logs and metrics from and also do some profiling.

## How to run
```bash
# Install dependencies
$ yarn

# Create Docker Kibana Image
$ docker-compose up -d

# Run APM-Server
$ cd apm-server
$ ./apm-server -e

# Run the project in development mode
$ yarn dev

# Run filebeat
$ cd filebeat
$ ./filebeat setup ./filebeat -e

```
