const elasticsearch = require('elasticsearch');


const getClient = function getClient() {
  const client = new elasticsearch.Client({
    host: 'localhost:9200'
  });

  return client;
}

module.exports = getClient;