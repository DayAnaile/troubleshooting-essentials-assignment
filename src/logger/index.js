const Winston = require('winston');
const WinstonLogStash = require('winston3-logstash-transport');

const logger = Winston.createLogger({
  format: Winston.format.combine(
    Winston.format.errors({ stack: true }),
    Winston.format.timestamp(),
    Winston.format.json()
  ),
  transports: [
      new Winston.transports.File({ filename: './logs/error.log', level: 'error' }),
      new Winston.transports.File({ filename: './logs/info.log', level: 'info' }),
  ],
});

logger.add(new WinstonLogStash({
  mode: 'udp',
  host: 'localhost:9200'
}));

logger.error('Some Error');

module.exports = logger;