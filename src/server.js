const { request, response } = require('express');
const getClient = require('./client/elasticsearch');
const logger = require('./logger/index');
const bodyParser = require('body-parser');
const apm = require('elastic-apm-node');

// APM initialization

apm.start({
  serviceName: 'elasticsearch',
  secretToken: '030BeZquHoy6CrajNQ',
  serverUrl: 'http://localhost:8200',
  active: true,
  environment: 'development'
})

// Express

const app = require('express')()
app.use(bodyParser.json());

app.get('/', async (request, response) => {

  const client = getClient();

  try {
    const result = await client.index({
      index: 'elastic_teste',
      type: 'type_elastic_teste',
      body: {
        user: 'UserTest',
        password: 'userpassword',
        email: 'user.test@email.com'
      }
    });
    if (response.status(200)){
      logger.info('Successful execution of request!');
      return response.json(result);
    }

  } catch (err) {
    response.status(400).send((err).message);
    logger.error(err);
    apm.captureError(err);
  }
})

app.listen(3000);